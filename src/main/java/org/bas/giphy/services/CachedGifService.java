package org.bas.giphy.services;

import org.bas.giphy.models.dto.GifQueryFilesDto;
import org.bas.giphy.models.dto.GifSavingDto;
import org.bas.giphy.models.dto.GifsQueryDto;
import org.bas.giphy.repositories.CachedGifRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@Service
public class CachedGifService {

    private final CachedGifRepository cachedGifRepository;

    private final GiphyHttpRequestService httpRequestService;

    private final GifResponseHelper gifResponseHelper;

    @Autowired
    public CachedGifService(CachedGifRepository cachedGifRepository, GiphyHttpRequestService httpRequestService,
                            GifResponseHelper gifResponseHelper) {
        this.cachedGifRepository = cachedGifRepository;
        this.httpRequestService = httpRequestService;
        this.gifResponseHelper = gifResponseHelper;
    }

    public List<GifsQueryDto> getByQuery(String query) {
        return cachedGifRepository.getAllByQuery(query);
    }

    public GifQueryFilesDto generateAndGetAll(String query) throws IOException {
        final var response = httpRequestService.sendRandomGifGenerationRequest(query);
        final var gifUrl = gifResponseHelper.generateGifUrl(response);
        final var gifId = gifResponseHelper.getResponseGifId(response);
        cachedGifRepository.save(new GifSavingDto(gifUrl, gifId, query));
        return cachedGifRepository.getByQuery(query);
    }

    public String generateNewGif(String query) throws IOException {
        final var response = httpRequestService.sendRandomGifGenerationRequest(query);
        final var gifUrl = gifResponseHelper.generateGifUrl(response);
        final var gifId = gifResponseHelper.getResponseGifId(response);
        return cachedGifRepository.save(new GifSavingDto(gifUrl, gifId, query));
    }

    public void deleteAll() {
        cachedGifRepository.deleteAll();
    }

}
