package org.bas.giphy.services;

import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;

@Service
public class HttpUrlConnectionService {

    public String readConnectionData(HttpURLConnection connection) throws IOException {
        try (final var in = new BufferedReader(new InputStreamReader(connection.getInputStream()))) {
            String inputLine;
            final var responseData = new StringBuilder();
            while ((inputLine = in.readLine()) != null) {
                responseData.append(inputLine);
            }
            return responseData.toString();
        }
    }

}
