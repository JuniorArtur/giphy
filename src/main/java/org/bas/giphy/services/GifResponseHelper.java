package org.bas.giphy.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

import java.net.MalformedURLException;
import java.net.URL;

@Component
public class GifResponseHelper {

    private final ObjectMapper mapper;

    public GifResponseHelper() {
        this.mapper = new ObjectMapper();
    }

    public URL generateGifUrl(String response) throws MalformedURLException, JsonProcessingException {
        final var jsonRoot = mapper.readValue(response, JsonNode.class);
        final var gifUrl = jsonRoot.get("url").get("images").get("original").asText();
        return new URL("https://i" + gifUrl.substring(gifUrl.indexOf(".giphy.com/")));
    }

    public String getResponseGifId(String response) throws JsonProcessingException {
        final var jsonRoot = mapper.readValue(response, JsonNode.class);
        return jsonRoot.get("id").asText();
    }

}
