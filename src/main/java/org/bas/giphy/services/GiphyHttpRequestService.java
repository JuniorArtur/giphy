package org.bas.giphy.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

@Service
public class GiphyHttpRequestService {

    private static final Logger logger = LoggerFactory.getLogger(GiphyHttpRequestService.class);

    @Value(value = "${giphy.api-url}")
    private String API_URL;

    @Value("${giphy.api-random-gif-url}")
    private String API_RANDOM_GIF_URL;

    @Value(value = "${giphy.api-key}")
    private String API_KEY;

    private final HttpUrlConnectionService connectionService;

    public GiphyHttpRequestService(HttpUrlConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public String sendRandomGifGenerationRequest(String tag) throws IOException {
        return sendGetRequest(generateRandomGifRequestUrl(tag));
    }

    private URL generateRandomGifRequestUrl(String tag) throws MalformedURLException {
        final var urlParams = "?tag=" + tag + "&api_key=" + API_KEY;
        return new URL(API_URL + API_RANDOM_GIF_URL + urlParams);
    }

    private String sendGetRequest(URL url) throws IOException {
        final var connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");
        connection.getResponseCode();
        final var response = connectionService.readConnectionData(connection);
        connection.disconnect();
        return response;
    }

}
