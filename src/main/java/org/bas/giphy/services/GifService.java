package org.bas.giphy.services;

import org.bas.giphy.repositories.GifRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GifService {

    private final GifRepository gifRepository;

    @Autowired
    public GifService(GifRepository gifRepository) {
        this.gifRepository = gifRepository;
    }

    public List<String> getAllGifs() {
        return gifRepository.getAllGifs();
    }

}
