package org.bas.giphy.services;

public class DirectoryNameValidator {

    private static final String FILE_REGEX = "^\\w$";

    private DirectoryNameValidator() {
    }

    public static boolean isValidName(String directory) {
        return directory.matches(FILE_REGEX);
    }

}
