package org.bas.giphy.services;

import org.bas.giphy.models.GifPathMapper;
import org.bas.giphy.models.dto.GifsQueryDto;
import org.bas.giphy.models.dto.UserHistoryDto;
import org.bas.giphy.repositories.CachedGifRepository;
import org.bas.giphy.repositories.CachedUserRepository;
import org.bas.giphy.repositories.UserDataRepository;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final CachedGifService cachedGifService;

    private final CachedGifRepository cachedGifRepository;

    private final UserDataRepository userDataRepository;

    private final CachedUserRepository cachedUserRepository;

    private final GifPathMapper gifPathMapper;

    private final Random random = new Random();

    public UserService(CachedGifService cachedGifService, CachedGifRepository cachedGifRepository,
                       UserDataRepository userDataRepository, CachedUserRepository cachedUserRepository,
                       GifPathMapper gifPathMapper) {
        this.cachedGifService = cachedGifService;
        this.cachedGifRepository = cachedGifRepository;
        this.userDataRepository = userDataRepository;
        this.cachedUserRepository = cachedUserRepository;
        this.gifPathMapper = gifPathMapper;
    }


    public List<GifsQueryDto> getByUser(String id) {
        return userDataRepository.getUserFiles(id)
                .stream()
                .map(gifPathMapper::mapGifFilesToRelativePaths)
                .collect(Collectors.toList());
    }

    public List<UserHistoryDto> getUserHistory(String id) {
        return userDataRepository.getUserHistory(id);
    }

    public String search(String id, String query, boolean force) {
        if (force) {
            final var diskFilesDto = userDataRepository.getUserFilesByQuery(id, query);
            final var diskFiles = diskFilesDto.getFiles();
            if (!diskFiles.isEmpty()) {
                final var file = diskFiles.get(random.nextInt(diskFiles.size()));
                final var path = gifPathMapper.mapGifFileToRelativePath(file);
                cachedUserRepository.save(id, query, path);
                return path;
            }
        }
        final var userGifs = cachedUserRepository.getUserPathsByQuery(id, query);
        if (!userGifs.isEmpty()) {
            return userGifs.get(random.nextInt(userGifs.size()));
        }
        return null;
    }

    public String generate(String id, String query, Boolean force) throws IOException {
        if (force == null || !force) {
            final var cachedFiles = cachedGifRepository.getByQuery(query).getFiles();
            if (!cachedFiles.isEmpty()) {
                for (File file : cachedFiles) {
                    String fileName = file.getName();
                    if (!userDataRepository.isUserHistoryExists(id, query, fileName)) {
                        return copyFileFromCache(id, query, file);
                    }
                }
            }
        }
        return generateNewAndCopy(id, query);
    }

    private String generateNewAndCopy(String id, String query) throws IOException {
        final var cacheFile = new File(cachedGifService.generateNewGif(query));
        return copyFileFromCache(id, query, cacheFile);
    }

    private String copyFileFromCache(String id, String query, File cacheFile) throws IOException {
        final var newFile = userDataRepository.add(id, query, cacheFile);
        final var newPath = gifPathMapper.mapGifFileToRelativePath(newFile);
        userDataRepository.appendHistory(id, query, newPath);
        cachedUserRepository.save(id, query, newPath);
        return newPath;
    }

    public void clearUserHistory(String id) {
        userDataRepository.clearUserHistory(id);
    }

    public void resetRam(String id, String query) {
        if (query == null) {
            cachedUserRepository.resetUser(id);
        } else {
            cachedUserRepository.resetUserQuery(id, query);
        }
    }

    public void clearUserData(String id) {
        cachedUserRepository.resetUser(id);
        userDataRepository.clear(id);
    }

}
