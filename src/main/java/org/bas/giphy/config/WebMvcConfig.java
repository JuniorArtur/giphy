package org.bas.giphy.config;

import org.bas.giphy.filters.HttpHeadersInterceptor;
import org.bas.giphy.filters.HttpRequestLoggingInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    private final HttpHeadersInterceptor httpHeadersInterceptor;
    private final HttpRequestLoggingInterceptor httpRequestLoggingInterceptor;

    @Autowired
    public WebMvcConfig(HttpHeadersInterceptor httpHeadersInterceptor,
                        HttpRequestLoggingInterceptor httpRequestLoggingInterceptor) {
        this.httpHeadersInterceptor = httpHeadersInterceptor;
        this.httpRequestLoggingInterceptor = httpRequestLoggingInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(httpHeadersInterceptor);
    }

}
