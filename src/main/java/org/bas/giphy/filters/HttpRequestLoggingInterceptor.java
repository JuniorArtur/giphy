package org.bas.giphy.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Configuration
public class HttpRequestLoggingInterceptor extends HandlerInterceptorAdapter {

    private static final Logger log = LoggerFactory.getLogger(HttpRequestLoggingInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        log.info("[{}], URI: {}, Params: {};", request.getMethod(), request.getRequestURI(), getParameters(request));
        return true;
    }

    private String getParameters(HttpServletRequest request) {
        final var paramsBuilder = new StringBuilder();
        final var parameters = request.getParameterNames();
        if (parameters != null) {
            while (parameters.hasMoreElements()) {
                final var key = parameters.nextElement();
                final var value = request.getParameter(key);
                paramsBuilder.append(paramsBuilder.length() > 1 ? "&" : "?").append(key).append("=").append(value);
            }
        }
        return paramsBuilder.toString();
    }

}
