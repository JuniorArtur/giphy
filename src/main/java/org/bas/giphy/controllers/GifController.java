package org.bas.giphy.controllers;

import org.bas.giphy.services.GifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/gifs")
public class GifController {

    private final GifService gifService;

    @Autowired
    public GifController(GifService gifService) {
        this.gifService = gifService;
    }

    @GetMapping
    public ResponseEntity<List<String>> getAll() {
        return new ResponseEntity<>(gifService.getAllGifs(), HttpStatus.OK);
    }

}
