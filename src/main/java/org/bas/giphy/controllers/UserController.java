package org.bas.giphy.controllers;

import org.bas.giphy.models.UserGenerationDto;
import org.bas.giphy.models.dto.GifsQueryDto;
import org.bas.giphy.models.dto.UserHistoryDto;
import org.bas.giphy.services.DirectoryNameValidator;
import org.bas.giphy.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}/all")
    public ResponseEntity<List<GifsQueryDto>> getUserGifs(@PathVariable String id) {
        if (!DirectoryNameValidator.isValidName(id)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(userService.getByUser(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/history")
    public ResponseEntity<List<UserHistoryDto>> getUserHistory(@PathVariable String id) {
        if (!DirectoryNameValidator.isValidName(id)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(userService.getUserHistory(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/search")
    public ResponseEntity<String> search(@PathVariable String id,
                                         @RequestParam String query,
                                         @RequestParam Map<String, String> params) {
        if (!DirectoryNameValidator.isValidName(id)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(userService.search(id, query, params.containsKey("force")), HttpStatus.OK);
    }

    @PostMapping("/{id}/generate")
    public ResponseEntity<String> generateUser(@PathVariable String id,
                                               @RequestBody UserGenerationDto userGenerationDto) throws IOException {
        if (!DirectoryNameValidator.isValidName(id)) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(userService.generate(id, userGenerationDto.getQuery(), userGenerationDto.isForce()),
                HttpStatus.OK
        );
    }

    @DeleteMapping("/{id}/history/clean")
    public ResponseEntity<HttpStatus> clearUserHistory(@PathVariable String id) {
        userService.clearUserHistory(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}/reset")
    public ResponseEntity<HttpStatus> deleteReset(@PathVariable String id, @RequestParam Map<String, String> params) {
        userService.resetRam(id, params.get("query"));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @DeleteMapping("/{id}/clean")
    public ResponseEntity<HttpStatus> clearAllData(@PathVariable String id) {
        userService.clearUserData(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
