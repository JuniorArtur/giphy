package org.bas.giphy.controllers;

import org.bas.giphy.models.dto.GifQueryFilesDto;
import org.bas.giphy.models.dto.GifsQueryDto;
import org.bas.giphy.services.CachedGifService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/cache")
public class CachedGifController {

    private final CachedGifService cachedGifService;

    @Autowired
    public CachedGifController(CachedGifService cachedGifService) {
        this.cachedGifService = cachedGifService;
    }

    @GetMapping
    public ResponseEntity<List<GifsQueryDto>> getCacheByQuery(@RequestParam Map<String, String> params) {
        return new ResponseEntity<>(cachedGifService.getByQuery(params.get("query")), HttpStatus.OK);
    }

    @PostMapping("/generate")
    public ResponseEntity<GifQueryFilesDto> generate(@RequestBody String query) throws IOException {
        return new ResponseEntity<>(cachedGifService.generateAndGetAll(query), HttpStatus.OK);
    }

    @DeleteMapping
    public ResponseEntity<HttpStatus> clear() {
        cachedGifService.deleteAll();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
