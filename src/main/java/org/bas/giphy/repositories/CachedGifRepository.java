package org.bas.giphy.repositories;

import org.bas.giphy.models.dto.GifQueryFilesDto;
import org.bas.giphy.models.dto.GifSavingDto;
import org.bas.giphy.models.dto.GifsQueryDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Repository
public class CachedGifRepository {

    @Value(value = "${app.cache-dir}")
    private String CACHE_DIRECTORY;

    private final GifRepository gifRepository;

    @Autowired
    public CachedGifRepository(GifRepository gifRepository) {
        this.gifRepository = gifRepository;
    }

    public GifQueryFilesDto getByQuery(String query) {
        return gifRepository.getFilesFromRootByQuery(new File(CACHE_DIRECTORY), query);
    }

    public List<GifsQueryDto> getAllByQuery(String query) {
        return gifRepository.getGifsByQuery(new File(CACHE_DIRECTORY), query);
    }

    public String save(GifSavingDto saveDto) throws IOException {
        return gifRepository.save(CACHE_DIRECTORY, saveDto);
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(new File(CACHE_DIRECTORY));
    }

}
