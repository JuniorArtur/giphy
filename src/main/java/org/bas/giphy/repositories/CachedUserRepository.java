package org.bas.giphy.repositories;

import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

@Repository
public class CachedUserRepository {

    private final Map<String, Map<String, List<String>>> cache = new ConcurrentHashMap<>();

    public List<String> getUserPathsByQuery(String id, String query) {
        final var userStorage = cache.get(id);
        if (userStorage == null) {
            return Collections.emptyList();
        }
        final var queryUserStorage = userStorage.get(query);
        return userStorage.get(query) == null ? Collections.emptyList() : new ArrayList<>(queryUserStorage);
    }

    public void save(String id, String query, String name) {
        final var userStorage = cache.computeIfAbsent(id, k -> new ConcurrentHashMap<>());
        final var queryUserStorage = userStorage.computeIfAbsent(query, k -> new CopyOnWriteArrayList<>());
        queryUserStorage.add(name);
    }

    public void resetUser(String id) {
        cache.remove(id);
    }

    public void resetUserQuery(String id, String query) {
        final var userStorage = cache.get(id);
        if (userStorage != null) {
            userStorage.remove(query);
        }
    }

}
