package org.bas.giphy.repositories;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
import org.bas.giphy.models.dto.GifQueryFilesDto;
import org.bas.giphy.models.dto.UserHistoryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.util.FileSystemUtils;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Repository
public class UserDataRepository {

    private static final Logger logger = LoggerFactory.getLogger(UserDataRepository.class);

    @Value(value = "${app.users-dir}")
    private String USERS_DIRECTORY;

    @Value(value = "${app.users-history-file}")
    private String USERS_HISTORY_FILE_NAME;

    private final GifRepository gifRepository;

    @Autowired
    public UserDataRepository(GifRepository gifRepository) {
        this.gifRepository = gifRepository;
    }

    public List<GifQueryFilesDto> getUserFiles(String id) {
        return gifRepository.getFilesFromRoot(Path.of(USERS_DIRECTORY, id).toFile());
    }

    public GifQueryFilesDto getUserFilesByQuery(String id, String query) {
        return gifRepository.getFilesFromRootByTag(Path.of(USERS_DIRECTORY, id).toFile(), query);
    }

    public List<UserHistoryDto> getUserHistory(String id) {
        final var historyFile = Path.of(USERS_DIRECTORY, id, USERS_HISTORY_FILE_NAME).toFile();
        try (final var csvReader = new CSVReader(new FileReader(historyFile))) {
            final var list = new ArrayList<UserHistoryDto>();
            String[] nextLine;
            while ((nextLine = csvReader.readNext()) != null) {
                list.add(new UserHistoryDto(nextLine[0], nextLine[1], nextLine[2]));
            }
            return list;
        } catch (Exception ex) {
            logger.error(ex.getMessage(), ex);
            return Collections.emptyList();
        }
    }

    public boolean isUserHistoryExists(String id, String tag, String fileName) {
        return Path.of(USERS_DIRECTORY, id, tag, fileName).toFile().exists();
    }

    public File add(String id, String query, File cacheFile) {
        final var userQueryDirectory = Path.of(USERS_DIRECTORY, id, query).toFile();
        userQueryDirectory.mkdirs();
        try {
            final var gifFile = new File(userQueryDirectory, cacheFile.getName());
            if (!gifFile.exists()) {
                Files.copy(cacheFile.toPath(), gifFile.toPath());
            }
            return gifFile;
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
            return null;
        }
    }

    public void appendHistory(String id, String tag, String userFilePath) throws IOException {
        final var historyPath = Path.of(USERS_DIRECTORY, id, USERS_HISTORY_FILE_NAME);
        final var historyFile = historyPath.toFile();
        try (final var csvWriter = historyFile.exists()
                ? new CSVWriter(new FileWriter(historyFile, true))
                : new CSVWriter(new FileWriter(historyFile))) {
            final var dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");
            final var data = new String[]{dtf.format(LocalDateTime.now()), tag, userFilePath};
            csvWriter.writeNext(data);
        }
    }

    public void clear(String id) {
        FileSystemUtils.deleteRecursively(new File(new File(USERS_DIRECTORY), id));
    }

    public void clearUserHistory(String id) {
        try {
            Files.delete(Path.of(USERS_DIRECTORY, id, USERS_HISTORY_FILE_NAME));
            logger.info("User id: {} files were deleted", id);
        } catch (IOException ex) {
            logger.error(ex.getMessage(), ex);
        }
    }

}
