package org.bas.giphy.repositories;

import org.bas.giphy.models.dto.GifQueryFilesDto;
import org.bas.giphy.models.dto.GifSavingDto;
import org.bas.giphy.models.dto.GifsQueryDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.Channels;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Repository
public class GifRepository {

    @Value(value = "${app.users-dir}")
    private String USERS_DIRECTORY;

    public List<String> getAllGifs() {
        return this.getUserGifs(new File(USERS_DIRECTORY));
    }

    public List<String> getUserGifs(File directory) {
        return Stream.of(Objects.requireNonNull(directory.listFiles()))
                .filter(File::isDirectory)
                .map(dir -> getGifsByQuery(dir, null))
                .flatMap(Collection::stream)
                .map(GifsQueryDto::getGifs)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public List<GifsQueryDto> getGifsByQuery(File directory, String query) {
        return Stream.of(Objects.requireNonNull(directory.listFiles()))
                .filter(File::isDirectory)
                .filter(file -> (query == null || file.getName().equals(query)))
                .map(this::getGifPathsFromDirectory)
                .collect(Collectors.toList());
    }

    public String save(String directory, GifSavingDto gifToSave) throws IOException {
        final var cacheDir = new File(directory);
        final var queryDir = new File(cacheDir, gifToSave.getTag());
        queryDir.mkdirs();
        final var fileToCreate = new File(queryDir, gifToSave.getId() + ".gif");
        if (!fileToCreate.exists()) {
            try (final var byteStream = Channels.newChannel(gifToSave.getUrl().openStream());
                 final var out = new FileOutputStream(fileToCreate)) {
                out.getChannel().transferFrom(byteStream, 0, Long.MAX_VALUE);
            }
        }
        return fileToCreate.getAbsolutePath();
    }

    private GifsQueryDto getGifPathsFromDirectory(File file) {
        return new GifsQueryDto(file.getName(),
                Stream.of(Objects.requireNonNull(file.listFiles()))
                        .filter(File::isFile)
                        .map(File::getAbsolutePath)
                        .filter(name -> name.endsWith(".gif"))
                        .collect(Collectors.toList())
        );
    }

    private GifQueryFilesDto getFilesFromQueryDirectory(File directory) {
        return new GifQueryFilesDto(directory.getName(),
                Stream.of(Objects.requireNonNull(directory.listFiles()))
                        .filter(File::isFile)
                        .filter(file -> file.getName().endsWith(".gif"))
                        .collect(Collectors.toList()));
    }

    public GifQueryFilesDto getFilesFromRootByTag(File directory, String query) {
        return getFilesFromQueryDirectory(new File(directory, query));
    }

    public List<GifQueryFilesDto> getFilesFromRoot(File directory) {
        return Stream.of(Objects.requireNonNull(directory.listFiles()))
                .filter(File::isDirectory)
                .map(this::getFilesFromQueryDirectory)
                .collect(Collectors.toList());
    }

    public GifQueryFilesDto getFilesFromRootByQuery(File directory, String query) {
        return getFilesFromQueryDirectory(new File(directory, query));
    }

}
