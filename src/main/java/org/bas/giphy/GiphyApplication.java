package org.bas.giphy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@PropertySource("classpath:dev.properties")
public class GiphyApplication {

    public static void main(String[] args) {
        SpringApplication.run(GiphyApplication.class, args);
    }

}
