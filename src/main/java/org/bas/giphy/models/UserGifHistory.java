package org.bas.giphy.models;

public class UserGifHistory {

    private final String date;

    private final String query;

    private final String gif;

    public UserGifHistory(String date, String query, String gif) {
        this.date = date;
        this.query = query;
        this.gif = gif;
    }

    public String getDate() {
        return date;
    }

    public String getQuery() {
        return query;
    }

    public String getGif() {
        return gif;
    }

}
