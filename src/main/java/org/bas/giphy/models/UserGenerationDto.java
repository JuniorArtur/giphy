package org.bas.giphy.models;

public class UserGenerationDto {

    private final String query;

    private final Boolean force;

    public UserGenerationDto(String query, Boolean force) {
        this.query = query;
        this.force = force;
    }

    public String getQuery() {
        return query;
    }

    public Boolean isForce() {
        return force;
    }

}
