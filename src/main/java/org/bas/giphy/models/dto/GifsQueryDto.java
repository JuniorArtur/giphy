package org.bas.giphy.models.dto;

import java.util.List;

public class GifsQueryDto {

    private final String query;

    private final List<String> gifs;

    public GifsQueryDto(String query, List<String> gifs) {
        this.query = query;
        this.gifs = gifs;
    }

    public String getQuery() {
        return query;
    }

    public List<String> getGifs() {
        return gifs;
    }

}
