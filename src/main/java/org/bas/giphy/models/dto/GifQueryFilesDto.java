package org.bas.giphy.models.dto;

import java.io.File;
import java.util.List;

public class GifQueryFilesDto {

    private final String name;

    private final List<File> files;

    public GifQueryFilesDto(String name, List<File> files) {
        this.name = name;
        this.files = files;
    }

    public String getName() {
        return name;
    }

    public List<File> getFiles() {
        return files;
    }

}
