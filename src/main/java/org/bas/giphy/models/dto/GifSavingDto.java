package org.bas.giphy.models.dto;

import java.net.URL;

public class GifSavingDto {

    private final URL url;
    private final String id;
    private final String tag;

    public GifSavingDto(URL url, String id, String tag) {
        this.url = url;
        this.id = id;
        this.tag = tag;
    }

    public URL getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }

    public String getTag() {
        return tag;
    }

}
