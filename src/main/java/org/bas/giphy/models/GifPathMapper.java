package org.bas.giphy.models;

import org.bas.giphy.models.dto.GifQueryFilesDto;
import org.bas.giphy.models.dto.GifsQueryDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.stream.Collectors;

@Component
public class GifPathMapper {

    @Value(value = "${app.users-dir}")
    private String USERS_DIRECTORY;

    public String mapGifFileToRelativePath(File file) {
        return mapFileToRelativePath(USERS_DIRECTORY, file);
    }

    public GifsQueryDto mapGifFilesToRelativePaths(GifQueryFilesDto gifQueryFilesDto) {
        return new GifsQueryDto(gifQueryFilesDto.getName(), gifQueryFilesDto.getFiles()
                .stream()
                .map(file -> mapFileToRelativePath(USERS_DIRECTORY, file))
                .collect(Collectors.toList()));
    }

    private String mapFileToRelativePath(String rootDirectory, File file) {
        return "/content/" + new File(rootDirectory).toURI().relativize(file.toURI()).toString();
    }

}
